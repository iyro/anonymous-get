#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <libgen.h>
#include <time.h>

#define LENGTH 512

struct packet {
	char url[256];
	int chainSize;
	char ipAddr[10][15];
	char portNo[10][6];
};

struct address {
	char ipAddr[15];
	char portNo[6];
};

struct address getNextAddress(struct packet *chainInfo) {
	struct address nextAddress;
	srand(time(NULL));
	int i = rand() % chainInfo->chainSize;
	
	strcpy (nextAddress.ipAddr, chainInfo->ipAddr[i]);
	strcpy (nextAddress.portNo, chainInfo->portNo[i]);

	chainInfo->chainSize--;

	while(i<chainInfo->chainSize) {
		strcpy(chainInfo->ipAddr[i], chainInfo->ipAddr[i+1]);
		strcpy(chainInfo->portNo[i], chainInfo->portNo[i+1]);
		i++;
	}
	
	memset(&(chainInfo->ipAddr[i]), '\0', sizeof(chainInfo->ipAddr[i]));
	memset(&(chainInfo->portNo[i]), '\0', sizeof(chainInfo->ipAddr[i]));
	
	return nextAddress;
}

int connectTo(struct address nextAddress) {
	struct addrinfo hints, *res;
	int sock;
	
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	if (getaddrinfo(nextAddress.ipAddr, nextAddress.portNo, &hints, &res) != 0) {
		perror("getaddrinfo() error!");
		return 1;
	}
	
	if ((sock = socket(PF_INET, SOCK_STREAM, res->ai_protocol)) < 0) {
		perror("Socket creation failed!");
		return 1;
	}
		
	if (connect(sock, res->ai_addr, res->ai_addrlen) < 0) {
		perror( "Connect failed!");
		close (sock);
		return 1;
	}
	
	freeaddrinfo (res);
	
	return sock;
}

char *findBasename (char url[256]) {
	int i=0;
	char *urlBasename = basename(url);
	char *index = "index.html";
	for(i=strlen(url) - 1; i>=0;i--) {
		if (url[i] == '/') {
			if (strcmp(&url[i+1], urlBasename) == 0) {
				return urlBasename;
			}
			else {
				return index;
			}
		}
	}
	return index;
}
				


