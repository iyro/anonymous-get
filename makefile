CC=gcc

CFLAGS = -Wall

all: awget ss

awget:
	$(CC) $(CFLAGS) awget.c -o awget

ss:
	$(CC) $(CFLAGS) -pthread ss.c -o ss

clean:
	rm -rf awget ss
