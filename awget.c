#include "awget.h"

char* chainFile = "chaingang.txt";

void print_help()
{
	fprintf(stdout, "Valid usages are : \n");
	fprintf(stdout, "  ./awget <URL> -c <Chain File Path>\n");
	fprintf(stdout, "  ./awget <URL>\t\t\t\t\t:\tInitiate Chat as Server\n");
	fprintf(stdout, "  ./awget -h\t\t\t\t\t:\tDisplay this Summary & exit\n\n");
	return;
}

int awget (char URL[256]) {
	FILE *fp;
	int i=0, awgetSocket, recvSize, writeSize;
	struct packet chainInfo;
	unsigned long int testip;
	struct address nextAddress;
	char buffer[LENGTH], command[128];

	fprintf(stdout, "awget:\n");
	
	strcpy(chainInfo.url, URL);
	fprintf(stdout, "Request: %s\n", chainInfo.url);
	
	fp = fopen(chainFile, "r");
	
	if (fp == NULL) {
		fprintf(stdout, "Can't open chain file %s!\n", chainFile);
		return 1;
	}
	
	fscanf(fp, "%d", &(chainInfo.chainSize));
	
	while(fscanf(fp, "%s %s", chainInfo.ipAddr[i], chainInfo.portNo[i]) != EOF && i < chainInfo.chainSize) {
		if (atoi(chainInfo.portNo[i]) > 0 && atoi(chainInfo.portNo[i]) < 65536 && inet_pton(AF_INET, chainInfo.ipAddr[i], &testip) != 0)
			i++;
		else {
			fprintf(stdout, "Invalid port number or Invalid IP at line no. %d\n", i+2);
			return 1;
		}
	}

	fclose(fp);
	
	fprintf(stdout, "chainlist is\n");
	for (i=0; i<chainInfo.chainSize; i++)
		fprintf(stdout, "%s, %s\n", chainInfo.ipAddr[i], chainInfo.portNo[i]);
		
	nextAddress = getNextAddress(&chainInfo);
	fprintf(stdout, "next SS is %s, %s\n", nextAddress.ipAddr, nextAddress.portNo);
	
	if ((awgetSocket = connectTo(nextAddress)) == 1)
		return 1;
		
	if (send(awgetSocket, &chainInfo, sizeof(chainInfo), 0) < 0) {
		perror("Send failed!");
		close (awgetSocket);
		return 1;
	}
	
	fp = fopen (findBasename(chainInfo.url), "wb");
	bzero(buffer, LENGTH);
	i=0;
	
	fprintf(stdout, "waiting for file...\n..\n");
	
	while ((recvSize = recv(awgetSocket, buffer, LENGTH, 0)) != 0) {
		i++;
        if(recvSize < 0) {
            perror("Receive file error!");
            close(awgetSocket);
            return 1;
        }
		else if(recvSize==0) {
			break;
		}
        writeSize = fwrite(buffer, sizeof(char), recvSize, fp);

        if(writeSize < recvSize) {
            perror("File write failed.\n");
            close(awgetSocket);
            fclose(fp);
			return 1;
        }
        bzero(buffer, LENGTH);
    }
    
    fclose(fp);
    close(awgetSocket);

    if (i==0) {
		sprintf(command, "rm -f %s", findBasename(chainInfo.url));
		system(command);
		fprintf(stdout, "No data received! Please check URL.\nGoodbye!\n");
		return 1;
	}
	else {    
		fprintf(stdout, "Received file %s\nGoodbye!\n", findBasename(chainInfo.url));
		return 0;
	}
}

int main (int argc, char* argv[]) {
	char opt;
	int hflag;

	if (argc != 4 && argc != 2) {
		fprintf(stdout, "Invalid Usage! Please refer to the guidelines below.\n\n");
		print_help();
		return 1;
	}

	while ((opt = getopt (argc, argv, ":c:h")) != -1) {
		switch (opt) {
			case 'h':
				hflag = 1;
				break;
			case 'c':
				chainFile = optarg;
				break;
			case '?':
				if (optopt == 'c')
					fprintf(stdout, "Error! Missing argument <Chain File Path> for option -%c. Please refer to the guidelines below.\n\n", optopt);
				else 
					if(optopt == 's')
						fprintf(stdout, "Error! Missing argument <IP address> for option -%c. Please refer to the guidelines below.\n\n", optopt);
					else
						fprintf(stdout, "Error! Unknown Option -%c. Please refer to the guidelines below.\n\n", optopt);
				print_help();
				return 1;
			default:
				fprintf(stdout, "Unknown error!");
				abort();
		}
	}

	if (hflag) {
		print_help();
		return 0;
	}

	return awget(argv[argc-1]);
}
