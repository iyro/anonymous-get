#include "awget.h"
#include <pthread.h>
#include <stdint.h>

int counter = 1;
pthread_mutex_t lock;

void print_help()
{
	fprintf(stdout, "Valid usages are : \n");
	fprintf(stdout, "  ./ss \t\t\t:\tInitiate relay server on random port \n");
	fprintf(stdout, "  ./ss <Port No.>\t:\tInitiate relay server on specific \n");
	fprintf(stdout, "  ./ss -h\t\t\t:\tDisplay this Summary & exit\n\n");
	return;
}

int initializeServer (char port[6]) {
	struct addrinfo hints, *res, *p;
	char host[INET_ADDRSTRLEN];
	struct sockaddr_in sin;
	int sock, yes = 1;
	socklen_t l;
	
	gethostname(host, sizeof(host));
	
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = 0;
	
	if (getaddrinfo(host, port, &hints, &res) != 0) {
		fprintf (stdout, "getaddrinfo() error!\n");
		return 1;
	}
	
	for(p = res; p != NULL; p = p->ai_next) {	
		if ((sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
			perror("Socket creation failed!");
			continue;
		}
		
		if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
			perror ("setsockopt failed!");
			return 1;
		}

		if (bind(sock, p->ai_addr, p->ai_addrlen) < 0) {	
			perror ("Socket bind failed!");
			close (sock);
			continue;
		}		
		
		break;
	}
	
	if (p == NULL) {
		perror("Could not bind to any socket!");
		close (sock);
		return 1;
	}
	
	freeaddrinfo (res);
		
	if (listen(sock, 5) == -1) {
		perror ("Listen failed!");
		close (sock);
		return 1;
	}
	
	l = sizeof(sin);
	if (getsockname(sock, (struct sockaddr *)&sin, &l) == -1) {
		perror ("getsockname failed!");
		close(sock);
		return 1;
	}
	
	fprintf(stdout, "ss %s (%s), %d:\n", host, inet_ntoa(sin.sin_addr), ntohs(sin.sin_port));
	
	return sock;
}

void *ssThread(void *sock) {
	pthread_mutex_lock (&lock);
	int requestNo = counter++;
	pthread_mutex_unlock (&lock);
	
	int incomingSocket, outgoingSocket, status, i, recvSize;
	struct packet chainInfo;
	struct address nextAddress;
	char buffer[LENGTH], command[256];
	FILE *fp;

	incomingSocket = (intptr_t) sock;

	memset(&chainInfo, 0, sizeof(chainInfo));

	if ((status = recv(incomingSocket, &chainInfo, sizeof(chainInfo), 0)) < 0) {
		perror("Receive error!\n");
		close (incomingSocket);
		pthread_exit(0);
	}

	fprintf(stdout, "Request %d: %s\n", requestNo, chainInfo.url);

	fprintf(stdout, "Request %d: chainlist is ", requestNo);
	if (chainInfo.chainSize == 0)
		fprintf(stdout, "empty\n");
	else {
		fprintf(stdout, "\n");
		for (i=0; i<chainInfo.chainSize; i++)
			fprintf(stdout, "%s, %s\n", chainInfo.ipAddr[i], chainInfo.portNo[i]);
	}

	if (status == 0) {
		fprintf(stdout, "Request %d: Connection closed by Client.\n", requestNo);
		close (incomingSocket);
		pthread_exit(0);
	}
	
	if (chainInfo.chainSize == 0) {
		sprintf(command, "rm -f %s", findBasename(chainInfo.url));
		system(command);

		sprintf(command, "wget -q -O %s %s", findBasename(chainInfo.url), chainInfo.url);
		fprintf(stdout, "Request %d: issuing wget for file %s\n..\n", requestNo, findBasename(chainInfo.url));
		if (system(command) == 0)
			fprintf(stdout, "Request %d: File received\n", requestNo);
		else {
			fprintf(stdout, "Request %d: wget failed!\n", requestNo);
			sprintf(command, "rm -f %s", findBasename(chainInfo.url));
			system(command);
			close(incomingSocket);
			pthread_exit(0);
		}

		if ((fp = fopen(findBasename(chainInfo.url), "rb")) == NULL) {
			fprintf(stdout, "Request %d: Unable to open file %s\n", requestNo, findBasename(chainInfo.url));
			close(incomingSocket);
			pthread_exit(0);
		}
		
		fprintf(stdout, "Request %d: Relaying file ...\n", requestNo);		
		while((recvSize = fread(buffer, sizeof(char), LENGTH, fp))>0) {
			if(send(incomingSocket, buffer, recvSize, 0) < 0) {
				perror("Failed to send file!");
				close(incomingSocket);
				pthread_exit(0);
			}
			bzero(buffer, LENGTH);
		}
		fclose(fp);
		sprintf(command, "rm -f %s", findBasename(chainInfo.url));
		system (command);	
	}
	else {
		nextAddress = getNextAddress(&chainInfo);
		fprintf(stdout, "Request %d: next SS is %s, %s\n", requestNo, nextAddress.ipAddr, nextAddress.portNo);

		if ((outgoingSocket = connectTo(nextAddress)) == 1)
			pthread_exit(0);

		if (send(outgoingSocket, &chainInfo, sizeof(chainInfo), 0) < 0) {
			perror("Send failed!");
			close (incomingSocket);
			close (outgoingSocket);
			pthread_exit(0);
		}

		fprintf(stdout, "Request %d: waiting for file...\n..\n", requestNo);

		i=0;
		while((recvSize = recv(outgoingSocket, buffer, LENGTH, 0)) != 0) {
			if(recvSize < 0) {
				perror("Receive file error!");
				close (incomingSocket);
				close (outgoingSocket);
				pthread_exit(0);
			}

			if (i==0) {
				fprintf(stdout, "Request %d: Relaying file ...\n", requestNo);
				i++;
			}

			if(send(incomingSocket, buffer, recvSize, 0) < 0) {
				perror("Failed to relay file!");
				close (incomingSocket);
				close (outgoingSocket);
				pthread_exit(0);
			}
			bzero(buffer, LENGTH);
		}
		
		if(i == 0) {
			fprintf(stdout, "Request %d: No File data received. Closing connections\n", requestNo);
			close(incomingSocket);
			close(outgoingSocket);
			pthread_exit(0);
		}
		close(outgoingSocket);
	}
	close(incomingSocket);

	fprintf(stdout, "Request %d: Goodbye!\n", requestNo);
	pthread_exit(0);
}

int main(int argc, char *argv[]) {
	int sock, incomingSocket;
	char port[6];
	struct sockaddr clientSockAddr;
	pthread_t thread_id;
	pthread_attr_t attr;
	socklen_t clientSockAddrLen = sizeof(clientSockAddr);
	
	pthread_attr_init(&attr);
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	
	strcpy(port, "\0");
	
	if (argc != 2 && argc != 1) {
		fprintf(stdout, "Invalid Usage! Please refer to the guidelines below.\n\n");
		print_help();
		return 1;
	}
	
	if (argc == 2) {
		if (atoi(argv[1]) > 0 && atoi(argv[1]) < 65536)
			strcpy(port, argv[1]);
		else {
			fprintf(stdout, "Invalid port number %s\n", port);
			print_help();
			return 1;
		}
	}

	if((sock = initializeServer(port)) == 1)
		return 1;
	
	while (1) {
		incomingSocket = accept(sock, &clientSockAddr, &clientSockAddrLen);
		
		if (incomingSocket < 0) {
			perror ("Accept failed!");
			close (incomingSocket);
			continue;
		}
		
		if(pthread_create(&thread_id, &attr, &ssThread, (void *) (intptr_t) incomingSocket) < 0) {
			perror("Could not create thread!");
			continue;
		}
	}
	
	return 0;
}
